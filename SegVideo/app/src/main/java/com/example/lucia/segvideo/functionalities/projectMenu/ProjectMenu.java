package com.example.lucia.segvideo.functionalities.projectMenu;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.example.lucia.segvideo.R;
import com.example.lucia.segvideo.functionalities.graphic.GraphicFragment;
import com.example.lucia.segvideo.functionalities.information.InformationFragment;
import com.example.lucia.segvideo.functionalities.project.ProjectActivity;
import com.example.lucia.segvideo.functionalities.segmentData.SegmentFragment;

import org.json.JSONObject;

public class ProjectMenu extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;



    private String projectName;
    private String algorithm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle != null){
            projectName = bundle.getString("name");
            algorithm = bundle.getString("algorithm");
        }

        Log.w("Project Name", projectName);
        Log.w("Choose Algortihm", algorithm);


    }



    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    GraphicFragment graphic = new GraphicFragment();
                    return graphic;
                case 1:

                    SegmentFragment segment = new SegmentFragment();
                    return segment;
                case 2:
                    InformationFragment information = new InformationFragment();
                    return information;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Graphic";
                case 1:
                    return "Segments";
                case 2:
                    return "Information";
            }
            return null;
        }
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}

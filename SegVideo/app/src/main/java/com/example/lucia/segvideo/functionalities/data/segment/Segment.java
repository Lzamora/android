package com.example.lucia.segvideo.functionalities.data.segment;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;

/**
 * Created by lucia on 10/11/17.
 */

public class Segment extends RealmObject {


    private int id;
    private String name;
    private String frameCut;
    private String duration;

    public Segment(){

    }

    public Segment(int id, JSONObject json) throws JSONException {
        this.id = id;
        this.name = json.getString("name");
        this.frameCut = json.getString("frame_cut");
        this.duration = json.getString("duration");

    }

    public Segment(String name) {
        this.name = name;
    }
    public Segment(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrameCut() {
        return frameCut;
    }

    public void setFrameCut(String frameCut) {
        this.frameCut = frameCut;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}

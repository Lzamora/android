package com.example.lucia.segvideo.functionalities.segmentData;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lucia.segvideo.R;
import com.example.lucia.segvideo.functionalities.projectMenu.ProjectMenu;

/**
 * Created by lucia on 10/1/17.
 */

public class SegmentFragment extends Fragment {

    private ProjectMenu mProject;
    private String projectName = "soccer1";
    private String algorithm = "original";



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_segments, container, false);
    }


}

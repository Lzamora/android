package com.example.lucia.segvideo.functionalities.base;

/**
 * Created by lucia on 10/1/17.
 */


public interface BasePresenter {

    void start();

}
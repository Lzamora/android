var express = require('express');
var router = express.Router();
var url = require('url');

/* GET users listing. */
router.get('/', function(req, res, next) {
    var queryData = url.parse(req.url, true).query;
    var mysql = require('mysql');
    var config = {
        host: "localhost",
        user: "root",
        password: "root",
        database: "segVideo"
    }
    var connection = mysql.createConnection(config);
    connection.connect();

    connection.query("SELECT name FROM project;",
        function(err,rows,fields){
            if (!err){
                if (rows.length > 0){
                    var row = {projects:rows}
                    res.send(row);
                } else {
                    var error = {msg:'No projects'}
                    res.send(error);
                }
            } else {
                var error = {msg:'Error'}
                res.send(error);
            }
        });
});

module.exports = router;
